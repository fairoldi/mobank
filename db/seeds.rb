# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

u = User.create :email => "fabio.airoldi@moviri.com", :password => "Moviri01!", :first_name => "Fabio", :last_name => "Airoldi", :address => "283 Franklin st\n 02110 Boston (MA)", :telephone_number => "+1 (857) 285-0172"
u.save!
u.accounts.create :account_type => "Checking", :balance => 123450, :account_number => "2748192", :routing_number => "343212"
u.accounts.create :account_type => "Savings", :balance => 5678900, :account_number => "2748193", :routing_number => "343212"

u.accounts[0].transactions.create :amount => 999, :description => "Subway", :effective_date => 6.days.ago
u.accounts[0].transactions.create :amount => 799, :description => "Subway", :effective_date => 3.days.ago

u.payees.create :name => "Giancarlo Airoldi", :address => "Via delle farfalle 4, Busto Arsizio, 21052 (VA), Italy", :description => "Dad"
u.payees.create :name => "Valentina Airoldi", :address => "Via delle farfalle 4, Busto Arsizio, 21052 (VA), Italy", :description => "Sister"

u.accounts[0].payments.create :amount => 10000, :payee => u.payees[0], :effective_date => 3.days.ago, :user => u
u.accounts[0].transactions.create :amount => 10000, :description => "Payment to" + u.payees[0].name , :effective_date => 2.days.ago 
u.accounts[0].payments.create :amount => 2000, :payee => u.payees[0], :effective_date => 7.days.ago, :user => u
u.accounts[0].transactions.create :amount => 2000, :description => "Payment to" + u.payees[0].name , :effective_date => 6.days.ago 
u.accounts[0].payments.create :amount => 50000, :payee => u.payees[1], :effective_date => 15.days.ago, :user => u
u.accounts[0].transactions.create :amount => 50000, :description => "Payment to" + u.payees[1].name , :effective_date => 14.days.ago 
