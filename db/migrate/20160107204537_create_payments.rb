class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.references :user, index: true, foreign_key: true
			t.references :account, index: true, foreign_key: true
      t.references :payee, index: true, foreign_key: true
      t.integer :amount
      t.datetime :effective_date

      t.timestamps null: false
    end
  end
end
