class CreateTransfers < ActiveRecord::Migration
  def change
    create_table :transfers do |t|
      t.references :from_account, index: true, foreign_key: true
      t.references :to_account, index: true, foreign_key: true
      t.integer :amount
      t.datetime :effective_ts

      t.timestamps null: false
    end
  end
end
