class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :first_name, null:false
      t.string :last_name, null:false
      t.timestamps null: false
			t.string :address
			t.string :telephone_number
    end
  end
end
