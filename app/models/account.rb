class Account < ActiveRecord::Base
  belongs_to :user
	has_many :payments, -> {order('effective_date DESC').limit(25) }
	has_many :transactions, -> { order('effective_date DESC').limit(25) }
	has_many :payees
end
