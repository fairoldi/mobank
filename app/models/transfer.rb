class Transfer < ActiveRecord::Base
  belongs_to :from_account
  belongs_to :to_account
end
