class Payment < ActiveRecord::Base
	belongs_to :user
	belongs_to :account
  belongs_to :payee
end
