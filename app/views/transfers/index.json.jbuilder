json.array!(@transfers) do |transfer|
  json.extract! transfer, :id, :from_account_id, :to_account_id, :amount, :effective_ts
  json.url transfer_url(transfer, format: :json)
end
