json.array!(@payments) do |payment|
  json.extract! payment, :id, :account_id, :payee_id, :amount, :submitted_ts, :effective_ts
  json.url payment_url(payment, format: :json)
end
