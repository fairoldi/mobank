json.array!(@accounts) do |account|
  json.extract! account, :id, :account_number, :routing_number, :account_type, :balance, :user_id
  json.url account_url(account, format: :json)
end
