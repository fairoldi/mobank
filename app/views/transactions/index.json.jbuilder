json.array!(@transactions) do |transaction|
  json.extract! transaction, :id, :account_id, :amount, :description, :efffective_date
  json.url transaction_url(transaction, format: :json)
end
