class PaymentsController < ApplicationController
	before_action :authenticate_user!
  before_action :set_payment, only: [:show, :edit, :update, :destroy]
	before_action	:set_section
	before_action	:set_user

  # GET /payments
  # GET /payments.json
  def index
    @payments = Payment.where(user: current_user).order(effective_date: :desc).limit(25)
  end

  # GET /payments/1
  # GET /payments/1.json
  def show
  end

  # GET /payments/new
  def new
    @payment = Payment.new
		@payment.effective_date = DateTime.now
  end

  # GET /payments/1/edit
  def edit
  end

  # POST /payments
  # POST /payments.json
  def create
    @payment = Payment.new(payment_params)
		#@payment.effective_date = Date.parse(payment_params['effective_date'])
		@payment.user = @user
		@payment.amount = 100 * @payment.amount

    respond_to do |format|
      if @payment.save
        format.html { redirect_to payments_path, notice: 'Payment was successfully created.' }
        format.json { render :show, status: :created, location: @payment }
      else
        format.html { render :new }
        format.json { render json: @payment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /payments/1
  # PATCH/PUT /payments/1.json
  def update
    respond_to do |format|
      if @payment.update(payment_params)
        format.html { redirect_to @payment, notice: 'Payment was successfully updated.' }
        format.json { render :show, status: :ok, location: @payment }
      else
        format.html { render :edit }
        format.json { render json: @payment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /payments/1
  # DELETE /payments/1.json
  def destroy
    @payment.destroy
    respond_to do |format|
      format.html { redirect_to payments_url, notice: 'Payment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_payment
      @payment = Payment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def payment_params
      params.require(:payment).permit(:account_id, :payee_id, :amount, :user_id, :effective_date)
    end

		def set_section
			@section = 'payments'
		end

		def set_user
			@user = current_user
		end
end
