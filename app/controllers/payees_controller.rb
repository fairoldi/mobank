class PayeesController < ApplicationController
	before_action :authenticate_user!
  before_action :set_payee, only: [:show, :edit, :update, :destroy]
	before_action :set_section

  # GET /payees
  # GET /payees.json
  def index
		@user = current_user
    @payees = @user.payees
  end

  # GET /payees/1
  # GET /payees/1.json
  def show
		@user = current_user
  end

  # GET /payees/new
  def new
		@user = current_user
    @payee = Payee.new
  end

  # GET /payees/1/edit
  def edit
		@user = current_user
  end

  # POST /payees
  # POST /payees.json
  def create
    @payee = Payee.new(payee_params)
		@payee.user_id = current_user.id

    respond_to do |format|
      if @payee.save
        format.html { redirect_to @payee, notice: 'Payee was successfully created.' }
        format.json { render :show, status: :created, location: @payee }
      else
        format.html { render :new }
        format.json { render json: @payee.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /payees/1
  # PATCH/PUT /payees/1.json
  def update
    respond_to do |format|
      if @payee.update(payee_params)
        format.html { redirect_to @payee, notice: 'Payee was successfully updated.' }
        format.json { render :show, status: :ok, location: @payee }
      else
        format.html { render :edit }
        format.json { render json: @payee.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /payees/1
  # DELETE /payees/1.json
  def destroy
    @payee.destroy
    respond_to do |format|
      format.html { redirect_to payees_url, notice: 'Payee was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_payee
      @payee = Payee.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def payee_params
      params.require(:payee).permit(:name, :description, :address, :uers_id)
    end

		def set_section
			@section = 'payments'
		end
end
