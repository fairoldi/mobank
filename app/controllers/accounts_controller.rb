class AccountsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_account, only: [:show, :edit, :update, :destroy]
	before_action :set_section

  # GET /accounts
  # GET /accounts.json
  def index
    @user = current_user
    @accounts = @user.accounts
  end

  # GET /accounts/1
  # GET /accounts/1.json
  def show
    @user = current_user
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_account
		  @account = Account.find(params[:id])
    end

		def set_section
			@section = 'accounts'
		end


end
